package com.devcamp.task5820.readlistdrink.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5820.readlistdrink.model.CDrink;
import com.devcamp.task5820.readlistdrink.repository.IDrinkRepository;

@CrossOrigin
@RestController
public class CDrinkController {
    @Autowired
    IDrinkRepository drinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getDrinks() {
        try{
            List<CDrink> listCustomer = new ArrayList<CDrink>();

            drinkRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
